package com.bca.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;

import adapter.HeaderAdapter;
import adapter.TokenAdapter;
import adapter.ClientHelperAdapter;
import adapter.DummyAdapter;
import adapter.EncryptAdapter;
import model.Globals;

@RestController
public class controller {
    final static Logger logger = Logger.getLogger(controller.class);

    @RequestMapping(value = "/ping", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody void GetPing() {
	return;
    }

    @RequestMapping(value = "/callAPI", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String CallAPI(@RequestBody model.mdlAPI mdlAPI) throws NamingException, KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, GeneralSecurityException, IOException {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;

	String jsonResult = "";
	Globals.keyAPI = mdlAPI.keyAPI;
	Gson gson = new Gson();
	// Get the base naming context from web.xml
	Context context = (Context) new InitialContext().lookup("java:comp/env");

	// Get a single value from web.xml
	String APIGatewayIPAddress = (String) context.lookup("param_ip_api_gw");
	String XBCAClientID = (String) context.lookup("param_xbca_clientid");
	String APISecret = (String) context.lookup("param_apisecret");
	String APIKey = (String) context.lookup("param_apikey");
	String clientIDKlikBCA = (String) context.lookup("param_client_id_kbi");

	String decryptedAPISecret = EncryptAdapter.decrypt(APISecret, Globals.keyAPI);
	String decryptedAPIKey = EncryptAdapter.decrypt(APIKey, Globals.keyAPI);
	String decryptedXBCAClientID = EncryptAdapter.decrypt(XBCAClientID, Globals.keyAPI);
	
	//subtitute API GW Client ID with KBI CLient ID if call API KBI Services 
	if (mdlAPI.urlAPI.contains("/internet-banking")){
	    decryptedXBCAClientID = clientIDKlikBCA;
	}

	// =========================================================================================
	// TODO: DUMMY ADAPTER.. REMOVE WHEN IMPLEMENTS IN UAT OR PRODUCTION!!
	// =========================================================================================
	String dummyJSONResult = DummyAdapter.GetWebServiceDummyResult(mdlAPI);
	if (!dummyJSONResult.equals("")) {
	    return dummyJSONResult;
	}
	// =========================================================================================

	model.mdlToken mdlToken = TokenAdapter.GetToken();
	String urlAPI = mdlAPI.urlAPI;
	String urlFinal = APIGatewayIPAddress + urlAPI;
	String methodAPI = mdlAPI.methodAPI;
	String jsonIn = mdlAPI.contentAPI == null ? "" : mdlAPI.contentAPI;
	String version = mdlAPI.version == null ? "-" : mdlAPI.version;

	String wsid = "";
	// check if there is additional header
	if (mdlAPI.header != null && !mdlAPI.header.equals("")) {
	    model.mdlAPIHeader mdlAPIHeader = gson.fromJson(mdlAPI.header, model.mdlAPIHeader.class);
	    wsid = mdlAPIHeader.wsid == null ? "" : mdlAPIHeader.wsid;
	}

	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();

	// HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();
	// ClientConfig config = new DefaultClientConfig();
	// SSLContext ctx = SSLContext.getInstance("SSL");
	// TrustManager[] trustAllCerts = { new InsecureTrustManagerAdapter() };
	// ctx.init(null, trustAllCerts, null);
	// config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, ctx));
	// Client client = Client.create(config);

	Client client = ClientHelperAdapter.createClient();
	WebResource webResource = client.resource(urlFinal);
	ClientResponse response = null;

	// canonicalize JSON (remove all whitespace like \r, \n, \t and space)
	String bodyToHash = jsonIn.replaceAll("\\s+", "");
	model.mdlHeader mdlHeader = HeaderAdapter.GetHeaders(methodAPI, urlAPI, mdlToken.access_token, bodyToHash, decryptedAPIKey, decryptedAPISecret);
	String authorization = "Bearer " + mdlToken.access_token;
	try {

	    Builder builder = webResource.type("application/json").header("Authorization", authorization).header("X-BCA-Key", mdlHeader.Key).header("X-BCA-Timestamp", mdlHeader.Timestamp).header("X-BCA-Signature", mdlHeader.Signature).header("client-id", decryptedXBCAClientID);
	    if (!wsid.equals("")) {
		builder.header("wsid", wsid);
	    }

	    if (methodAPI.equalsIgnoreCase("POST")) {
		response = builder.post(ClientResponse.class, jsonIn);
	    } else if (methodAPI.equalsIgnoreCase("GET")) {
		response = builder.get(ClientResponse.class);
	    } else if (methodAPI.equalsIgnoreCase("PUT")) {
		response = builder.put(ClientResponse.class, jsonIn);
	    } else if (methodAPI.equalsIgnoreCase("DELETE")) {
		response = builder.delete(ClientResponse.class, jsonIn);
	    }

	    jsonResult = response.getEntity(String.class);
	    // check if authorized or not.. if not authorized, get new token and then call API again
	    String responseStatus = Integer.toString(response.getStatus());

	    if (responseStatus.equalsIgnoreCase("401")) {
		try {
		    mdlErrorSchema = gson.fromJson(jsonResult, model.mdlErrorSchema.class);
		} catch (Exception e) {
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    logger.error("FAILED = ResponseTime : " + elapsedTime + ", version = " + version + ", URL :" + urlFinal + ", method: " + methodAPI + ", jsonIn:" + jsonIn + ", Authorization : " + authorization + ", X-BCA-Key:" + mdlHeader.Key + ", X-BCA-Timestamp:" + mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature + ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonOut:" + jsonResult + "Exception:" + e.toString(), e);
		    jsonResult = "{\"Error\":\"" + e.toString() + "\"}";
		}

		if (mdlErrorSchema.ErrorCode != null && (mdlErrorSchema.ErrorCode.equalsIgnoreCase("ESB-14-009") || mdlErrorSchema.ErrorCode.equalsIgnoreCase("ESB-17-001"))) {
		    mdlToken = TokenAdapter.GetNewToken();
		    mdlHeader = HeaderAdapter.GetHeaders(methodAPI, urlAPI, mdlToken.access_token, bodyToHash, decryptedAPIKey, decryptedAPISecret);
		    authorization = "Bearer " + mdlToken.access_token;
		    builder = webResource.type("application/json")
			    .header("Authorization", authorization)
			    .header("X-BCA-Key", mdlHeader.Key)
			    .header("X-BCA-Timestamp", mdlHeader.Timestamp)
			    .header("X-BCA-Signature", mdlHeader.Signature)
			    .header("client-id", decryptedXBCAClientID);
		    if (!wsid.equals("")) {
			builder.header("wsid", wsid);
		    }

		    if (methodAPI.equalsIgnoreCase("POST")) {
			response = builder.post(ClientResponse.class, jsonIn);
		    } else if (methodAPI.equalsIgnoreCase("GET")) {
			response = builder.get(ClientResponse.class);
		    } else if (methodAPI.equalsIgnoreCase("PUT")) {
			response = builder.put(ClientResponse.class, jsonIn);
		    } else if (methodAPI.equalsIgnoreCase("DELETE")) {
			response = builder.delete(ClientResponse.class, jsonIn);
		    }
		    jsonResult = response.getEntity(String.class);
		    responseStatus = Integer.toString(response.getStatus());
		}
	    }
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.info("SUCCESS = ResponseTime : " + elapsedTime + ", ResponseStatus : " + responseStatus + ", version = " + version + ", callAPI = URL :" + urlFinal + ", method: " + methodAPI + ", X-BCA-Key:" + mdlHeader.Key + ", X-BCA-Timestamp:" + mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature + ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonIn:" + jsonIn + ", jsonOut:" + jsonResult);
	} catch (Exception ex) {
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.error("FAILED = ResponseTime : " + elapsedTime + ", version = " + version + ", URL :" + urlFinal + ", method: " + methodAPI + ", jsonIn:" + jsonIn + ", Authorization : " + authorization + ", X-BCA-Key:" + mdlHeader.Key + ", X-BCA-Timestamp:" + mdlHeader.Timestamp + ", X-BCA-Signature:" + mdlHeader.Signature + ", X-BCA-ClientID:" + decryptedXBCAClientID + ", jsonOut:" + jsonResult + "Exception:" + ex.toString(), ex);
	    jsonResult = "{\"Error\":\"" + ex.toString() + "\"}";
	}

	return jsonResult;
    }

    @RequestMapping(value = "/callAPIBdsWeb", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String CallAPIBDSWeb(@RequestBody model.mdlAPIBDSWeb mdlAPIBDSWeb) throws NamingException, KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, GeneralSecurityException, IOException {
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;

	String jsonResult = "";
	Globals.keyAPI = mdlAPIBDSWeb.keyAPI;
	Gson gson = new Gson();
	// Get the base naming context from web.xml
	Context context = (Context) new InitialContext().lookup("java:comp/env");

	// Get a single value from web.xml
	String APIGatewayIPAddress = (String) context.lookup("param_ip_api_gw");
	String userDomain = (String) context.lookup("param_user_domain");
	String appID = (String) context.lookup("param_app_id");

	// =========================================================================================
	// TODO: DUMMY ADAPTER.. REMOVE WHEN IMPLEMENTS IN UAT OR PRODUCTION!!
	// =========================================================================================
	String dummyJSONResult = DummyAdapter.GetBDSDummyResult(mdlAPIBDSWeb);
	if (!dummyJSONResult.equals("")) {
	    return dummyJSONResult;
	}
	// =========================================================================================

	model.mdlToken mdlToken = TokenAdapter.GetToken();
	String urlAPI = mdlAPIBDSWeb.urlAPI;
	String urlFinal = APIGatewayIPAddress + urlAPI;
	String methodAPI = mdlAPIBDSWeb.methodAPI;
	String jsonIn = mdlAPIBDSWeb.contentAPI == null ? "" : mdlAPIBDSWeb.contentAPI;
	String jsonInLog = "";
	if (mdlAPIBDSWeb.urlAPI.contains("/pre-din/") && mdlAPIBDSWeb.methodAPI.equalsIgnoreCase("POST") && !mdlAPIBDSWeb.contentAPI.equals("")) {
	    model.mdlPreDIN mdlPreDIN = gson.fromJson(mdlAPIBDSWeb.contentAPI, model.mdlPreDIN.class);
	    jsonInLog = gson.toJson(mdlPreDIN);
	} else {
	    jsonInLog = mdlAPIBDSWeb.contentAPI;
	}
	String version = mdlAPIBDSWeb.version == null ? "-" : mdlAPIBDSWeb.version;

	model.mdlBDSWeb mdlBDSWeb = gson.fromJson(mdlAPIBDSWeb.contentBDS, model.mdlBDSWeb.class);
	String userID = mdlBDSWeb.user_id == null ? "" : mdlBDSWeb.user_id;
	String customerName = mdlBDSWeb.customer_name == null ? "" : mdlBDSWeb.customer_name;
	String birthDate = mdlBDSWeb.birth_date == null ? "" : mdlBDSWeb.birth_date;
	String country = mdlBDSWeb.country == null ? "" : mdlBDSWeb.country;
	String job1 = mdlBDSWeb.job1 == null ? "" : mdlBDSWeb.job1;
	String job2 = mdlBDSWeb.job2 == null ? "" : mdlBDSWeb.job2;
	String job3 = mdlBDSWeb.job3 == null ? "" : mdlBDSWeb.job3;
	String nik = mdlBDSWeb.nik == null ? "" : mdlBDSWeb.nik;

	model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();

	Client client = ClientHelperAdapter.createClient();
	WebResource webResource = client.resource(urlFinal);
	ClientResponse response = null;

	String authorization = "Bearer " + mdlToken.access_token;
	try {

	    Builder builder = webResource.type("application/json").header("Authorization", authorization).header("app-id", appID).header("user-domain", userDomain).header("user-id", userID);
	    if (!customerName.equals("")) {
		builder.header("customer-name", customerName);
	    }
	    if (!customerName.equals("")) {

	    }
	    if (!birthDate.equals("")) {
		builder.header("birth-date", birthDate);
	    }
	    if (!country.equals("")) {
		builder.header("country-code", country);
	    }
	    if (!job1.equals("")) {
		builder.header("job-1", job1);
	    }
	    if (!job2.equals("")) {
		builder.header("job-2", job2);
	    }
	    if (!job3.equals("")) {
		builder.header("job-3", job3);
	    }
	    if (!nik.equals("")) {
		builder.header("nik", nik);
	    }

	    if (methodAPI.equalsIgnoreCase("POST")) {
		response = builder.post(ClientResponse.class, jsonIn);
	    } else if (methodAPI.equalsIgnoreCase("GET")) {
		response = builder.get(ClientResponse.class);
	    } else if (methodAPI.equalsIgnoreCase("PUT")) {
		response = builder.put(ClientResponse.class, jsonIn);
	    } else if (methodAPI.equalsIgnoreCase("DELETE")) {
		response = builder.delete(ClientResponse.class, jsonIn);
	    }

	    jsonResult = response.getEntity(String.class);
	    String responseStatus = Integer.toString(response.getStatus());

	    if (responseStatus.equalsIgnoreCase("401")) {
		try {
		    mdlErrorSchema = gson.fromJson(jsonResult, model.mdlErrorSchema.class);
		} catch (Exception e) {
		    stopTime = System.currentTimeMillis();
		    elapsedTime = stopTime - startTime;
		    logger.error("FAILED CallAPIBDSWeb = ResponseTime : " + elapsedTime + ", version = " + version + ", URL :" + urlFinal + ", method: " + methodAPI + ", jsonIn:" + jsonInLog + ", Authorization : " + authorization + ", user-id:" + userID + ", user-domain:" + userDomain + ", app-id:" + appID + ", jsonOut:" + jsonResult + "Exception:" + e.toString(), e);
		    jsonResult = "{\"Error\":\"" + e.toString() + "\"}";
		}

		if (mdlErrorSchema.ErrorCode != null && (mdlErrorSchema.ErrorCode.equalsIgnoreCase("ESB-14-009") || mdlErrorSchema.ErrorCode.equalsIgnoreCase("ESB-17-001"))) {
		    mdlToken = TokenAdapter.GetNewToken();
		    authorization = "Bearer " + mdlToken.access_token;
		    builder = webResource.type("application/json").header("Authorization", authorization).header("app-id", appID).header("user-domain", userDomain).header("user-id", userID);

		    if (!customerName.equals("")) {
			builder.header("customer-name", customerName).header("birth-date", birthDate).header("country", country).header("job1", job1).header("job2", job2).header("job3", job3);
		    }

		    if (methodAPI.equalsIgnoreCase("POST")) {
			response = builder.post(ClientResponse.class, jsonIn);
		    } else if (methodAPI.equalsIgnoreCase("GET")) {
			response = builder.get(ClientResponse.class);
		    } else if (methodAPI.equalsIgnoreCase("PUT")) {
			response = builder.put(ClientResponse.class, jsonIn);
		    } else if (methodAPI.equalsIgnoreCase("DELETE")) {
			response = builder.delete(ClientResponse.class, jsonIn);
		    }
		    jsonResult = response.getEntity(String.class);
		    responseStatus = Integer.toString(response.getStatus());
		}
	    }
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    // to handle pre-DIN logging
	    mdlAPIBDSWeb.contentAPI = jsonInLog;
	    logger.info("SUCCESS CallAPIBDSWeb = ResponseTime : " + elapsedTime + ", ResponseStatus : " + responseStatus + ", version = " + version + ", callAPI = URL :" + urlFinal + ", method: " + methodAPI + ", user-id:" + userID + ", user-domain:" + userDomain + ", app-id:" + appID + ", mdlBDSWeb : " + gson.toJson(mdlBDSWeb) + ", jsonIn:" + jsonInLog + ", jsonOut:" + jsonResult);
	} catch (Exception ex) {
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    // to handle pre-DIN logging
	    mdlAPIBDSWeb.contentAPI = jsonInLog;
	    logger.error("FAILED CallAPIBDSWeb = ResponseTime : " + elapsedTime + ", version = " + version + ", URL :" + urlFinal + ", method: " + methodAPI + ", jsonIn:" + jsonIn + ", Authorization : " + authorization + ", user-id:" + userID + ", user-domain:" + userDomain + ", app-id:" + appID + ", mdlBDSWeb : " + gson.toJson(mdlBDSWeb) + ", jsonOut:" + jsonResult + "Exception:" + ex.toString(), ex);
	    jsonResult = "{\"Error\":\"" + ex.toString() + "\"}";
	}

	return jsonResult;
    }

    @RequestMapping(value = "/callAPIApp", method = RequestMethod.POST, consumes = "application/json", headers = "Accept=application/json")
    public @ResponseBody String CallAPIApp(@RequestBody model.mdlAPI mdlAPI) throws NamingException, KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, GeneralSecurityException, IOException {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	long startTime = System.currentTimeMillis();
	long stopTime = 0;
	long elapsedTime = 0;

	String jsonResult = "";
	Globals.appKeyAPI = mdlAPI.keyAPI;
	// Get the base naming context from web.xml
	Context context = (Context) new InitialContext().lookup("java:comp/env");

	// Get a single value from web.xml
	String appServiceIPAddress = (String) context.lookup("param_ip_app_service");
	String urlAPI = mdlAPI.urlAPI;
	String urlFinal = appServiceIPAddress + urlAPI;
	String methodAPI = mdlAPI.methodAPI;
	String jsonIn = mdlAPI.contentAPI == null ? "" : mdlAPI.contentAPI;
	String version = mdlAPI.version == null ? "-" : mdlAPI.version;
	
	String dummyJSONResult = DummyAdapter.GetAppDummyResult(mdlAPI);
	if (!dummyJSONResult.equals("")) {
	    return dummyJSONResult;
	}

	Client client = ClientHelperAdapter.createClient();
	WebResource webResource = client.resource(urlFinal);
	ClientResponse response = null;

	String clientID = HeaderAdapter.getAppClientID();
	try {

	    Builder builder = webResource.type("application/json").header("client-id", clientID);
	    if (methodAPI.equalsIgnoreCase("POST")) {
		response = builder.post(ClientResponse.class, jsonIn);
	    } else if (methodAPI.equalsIgnoreCase("GET")) {
		response = builder.get(ClientResponse.class);
	    } else if (methodAPI.equalsIgnoreCase("PUT")) {
		response = builder.put(ClientResponse.class, jsonIn);
	    } else if (methodAPI.equalsIgnoreCase("DELETE")) {
		response = builder.delete(ClientResponse.class, jsonIn);
	    }

	    jsonResult = response.getEntity(String.class);
	    String responseStatus = Integer.toString(response.getStatus());
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.info("API : " + functionName + ", SUCCESS = ResponseTime : " + elapsedTime + ", ResponseStatus : " + responseStatus + ", version = " + version + ", callAPI = URL :" + urlFinal + ", method: " + methodAPI + ", ClientID:" + clientID + ", jsonIn:" + jsonIn + ", jsonOut:" + jsonResult);
	} catch (Exception ex) {
	    stopTime = System.currentTimeMillis();
	    elapsedTime = stopTime - startTime;
	    logger.error("API : " + functionName + ", FAILED = ResponseTime : " + elapsedTime + ", version = " + version + ", URL :" + urlFinal + ", method: " + methodAPI + ", jsonIn:" + jsonIn + ", ClientID : " + clientID + ", jsonOut:" + jsonResult + "Exception:" + ex.toString(), ex);
	    jsonResult = "{\"Error\":\"" + ex.toString() + "\"}";
	}

	return jsonResult;
    }
}
