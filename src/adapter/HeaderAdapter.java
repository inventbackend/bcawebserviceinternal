package adapter;

import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.naming.Context;
import javax.naming.InitialContext;
import org.apache.log4j.Logger;
import model.Globals;

public class HeaderAdapter {
    final static Logger logger = Logger.getLogger(HeaderAdapter.class);

    public static model.mdlHeader GetHeaders(String httpMethod, String url, String token, String body, String apiKey, String apiSecret) {
	model.mdlHeader mdlHeader = new model.mdlHeader();
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	Date date = new Date();
	String timestamp = dateFormat.format(date);
	// encrypt body content with SHA256 and change it into lowercase
	String hashedBody = sha256(body).toLowerCase();
	String stringToSign = httpMethod + ":" + url + ":" + token + ":" + hashedBody + ":" + timestamp;
	String signature = generateHmacSHA256Signature(apiSecret, stringToSign);
	mdlHeader.Timestamp = timestamp;
	mdlHeader.Key = apiKey;
	mdlHeader.Signature = signature;

	return mdlHeader;
    }

    public static String getAppClientID() {
	String functionName = Thread.currentThread().getStackTrace()[1].getMethodName();
	String appClientID = "";
	String decryptedAppClientID = "";
	try {
	    Context context = (Context) new InitialContext().lookup("java:comp/env");
	    appClientID = (String) context.lookup("param_app_clientid");
	    decryptedAppClientID = EncryptAdapter.decrypt(appClientID, Globals.keyAPI);

	} catch (Exception ex) {
	    logger.error("FAILED. Function : " + functionName + ", Exception:" + ex.toString(), ex);
	}
	return decryptedAppClientID;
    }

    public static String sha256(String base) {
	try {
	    MessageDigest digest = MessageDigest.getInstance("SHA-256");
	    byte[] hash = digest.digest(base.getBytes("UTF-8"));
	    StringBuffer hexString = new StringBuffer();
	    for (int i = 0; i < hash.length; i++) {
		String hex = Integer.toHexString(0xff & hash[i]);
		if (hex.length() == 1)
		    hexString.append('0');
		hexString.append(hex);
	    }
	    return hexString.toString();
	} catch (Exception ex) {
	    throw new RuntimeException(ex);
	}
    }

    public static String generateHmacSHA256Signature(String key, String data)
    // throws GeneralSecurityException, IOException
    {
	String algorithm = "HmacSHA256"; // OPTIONS= HmacSHA512, HmacSHA256, HmacSHA1, HmacMD5
	String hash = "";
	try {
	    // 1. Get an algorithm instance.
	    Mac sha256_hmac = Mac.getInstance(algorithm);

	    // 2. Create secret key.
	    SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);

	    // 3. Assign secret key algorithm.
	    sha256_hmac.init(secret_key);

	    // Encode the string into bytes using utf-8 and digest it
	    byte[] digest = sha256_hmac.doFinal(data.getBytes("UTF-8"));

	    // Convert digest into a hex string
	    hash = byteArrayToHex(digest);

	} catch (Exception e) {

	}
	return hash;
    }

    public static String byteArrayToHex(byte[] a) {
	StringBuilder sb = new StringBuilder(a.length * 2);
	for (byte b : a)
	    sb.append(String.format("%02x", b));
	return sb.toString();
    }
}
