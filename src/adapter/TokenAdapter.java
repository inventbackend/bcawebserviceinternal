package adapter;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.representation.Form;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.bca.controller.controller;
import com.google.gson.Gson;

import model.Globals;
//import adapter.ClientHelperAdapter;
/**
 * Documentation
 *
 */
public class TokenAdapter {
    final static Logger logger = Logger.getLogger(controller.class);
    static Gson gson = new Gson();
    
    public static model.mdlToken GetToken() {
	model.mdlToken mdlToken = new model.mdlToken();

	if (Globals.token == null || Globals.token.equalsIgnoreCase("")) {
	    try {
		mdlToken = GetNewToken();
	    } catch (Exception ex) {
		Globals.token = "";
	    }
	} else {
	    try {
		Date dateNow = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date dateToken = df.parse(Globals.tokenDate.replace("T", " "));
		long toMinute = 1000 * 60;
		int dateDiff = (int) (dateNow.getTime() - dateToken.getTime());
		int diffMinute = (int) (dateDiff / toMinute);

		if (diffMinute >= 25) {
		    mdlToken = GetNewToken();
		} else {
		    mdlToken.access_token = Globals.token;
		}
	    } catch (Exception ex) {

	    }
	}
	return mdlToken;
    }

    public static model.mdlToken GetNewToken() throws KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, 
    BadPaddingException, GeneralSecurityException, IOException, NamingException {
	model.mdlToken mdlToken = new model.mdlToken();
	String jsonOut = "";

	// Get the base naming context from web.xml
	Context context = (Context) new InitialContext().lookup("java:comp/env");

	// Get a single value from web.xml
	String TokenIPAddress = (String) context.lookup("param_ip_token");
	String ClientID = (String) context.lookup("param_clientid");
	String ClientSecret = (String) context.lookup("param_clientsecret");

	// decrypt key
	String decryptedClientID = EncryptAdapter.decrypt(ClientID, Globals.keyAPI);
	String decryptedClientSecret = EncryptAdapter.decrypt(ClientSecret, Globals.keyAPI);
	
//	HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();	
//	ClientConfig config = new DefaultClientConfig();
//	SSLContext ctx = SSLContext.getInstance("SSL");
//	TrustManager[] trustAllCerts = { new InsecureTrustManagerAdapter() };
//	ctx.init(null, trustAllCerts, null);
//	config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, ctx));
//	Client client = Client.create(config);
	
	Client client = ClientHelperAdapter.createClient();

	String urlAPI = TokenIPAddress;
	String authorizationKey = ClientIdAdapter.getAuthorizationKey(decryptedClientID, decryptedClientSecret);
	WebResource webResource = client.resource(urlAPI);

	// add form variables
	Form form = new Form();
	form.add("grant_type", "client_credentials");
	
	try {
	    Builder builder = webResource.type("application/x-www-form-urlencoded")
			 .header("Authorization", "Basic " + authorizationKey);
	    ClientResponse response = builder.post(ClientResponse.class, form);

	    jsonOut = response.getEntity(String.class);
	    mdlToken = gson.fromJson(jsonOut, model.mdlToken.class);
	    logger.info("SUCCESS GetNewToken = URL :" + urlAPI + ", authorizationKey:" + authorizationKey 
		    	+ ", ClientID:" + decryptedClientID + ", ClientSecret:" + decryptedClientSecret + ", jsonOut:" + jsonOut);
	} catch (Exception ex) {
	    mdlToken.access_token = "";
	    logger.error("FAILED = URL :" + urlAPI + ", authorizationKey:" + authorizationKey 
		    	+ ", ClientID:" + decryptedClientID + ", ClientSecret:" + decryptedClientSecret + ", Exception:" + ex.toString(), ex);
	}
	Globals.token = mdlToken.access_token;
	Globals.tokenDate = LocalDateTime.now().toString();
	return mdlToken;
    }
}
