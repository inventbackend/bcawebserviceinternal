package model;

import com.google.gson.annotations.SerializedName;

public class mdlToken {
    public String access_token;
    public String expires_in;
    public String refresh_expires_in;
    public String refresh_token;
    public String token_type;
    public String id_token;
    
    @SerializedName("not-before-policy")
    public String not_before_policy;

    public String session_state;
}
