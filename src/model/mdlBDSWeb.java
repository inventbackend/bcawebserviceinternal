package model;

import com.google.gson.annotations.SerializedName;

public class mdlBDSWeb {
    @SerializedName(value="user-id")
    public String user_id; // WSID = 0020001E, user_id = 001E
    @SerializedName(value="customer-name")
    public String customer_name;
    @SerializedName(value="birth-date")
    public String birth_date;
    public String country;
    public String job1;
    public String job2;
    public String job3;
    public String nik;
}
